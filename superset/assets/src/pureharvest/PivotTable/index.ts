import { ChartMetadata, ChartPlugin } from '@superset-ui/chart';
import { t } from '@superset-ui/translation';
import transformProps from './transformProps';

const metadata = new ChartMetadata({
  description: '',
  name: t('Pivot Table'),
  thumbnail: '',
  useLegacyApi: true,
});

export default class PivotTableChartPlugin extends ChartPlugin {
  constructor() {
    super({
      loadChart: () => import('./PivotTable'),
      metadata,
      transformProps,
    });
  }
}
