import * as classNames from 'classnames';
import * as React from 'react';

import CalculatedMetric from 'src/explore/CalculatedMetric';

import { PivotCell, PivotCellProps } from './PivotCell';
import { PivotRowHeaderCell, PivotRowHeaderCellProps } from './PivotRowHeaderCell';
import { ProjectedRow } from './projection';
import { Metric, MetricDisplayOptions } from './transformProps';

export interface PivotTableFooterProps {
  metrics: Array<Metric | CalculatedMetric>;
  metricPosition: number;
  columnFormats: { [key: string]: string; };
  displayOptions?: { [key: string]: MetricDisplayOptions };
  defaultNumberFormat?: string;
  className?: string;
  width?: number;
  totals: ProjectedRow[];
}

function mapHeaderCell(props: PivotTableFooterProps) {
  const numberOfRows = props.totals.length;

  return (row: ProjectedRow, rowNumber: number): PivotRowHeaderCellProps[] => {

    const key = row.index[row.index.length - 1];
    const showKey = numberOfRows === 1 || rowNumber === 0;
    const style = {
      bottom: `${(numberOfRows - rowNumber - 1) * 28}px`,
    };

    const commonProps: PivotRowHeaderCellProps[] = [{
      className: classNames({
        'first-in-group': showKey,
        'row-header': true,
      }),
      columnIndex: [],
      rowIndex: row.index,
      style,
      value: showKey ? key : '',
    }];

    if (!row.metric) {
      return commonProps;
    }

    const metricProps: PivotRowHeaderCellProps = {
      className: classNames({
        'row-header': true,
      }),
      columnIndex: [],
      rowIndex: row.index,
      style,
      value: row.metric,
    };

    return commonProps.concat(metricProps);
  };
}

function mapValueCells(props: PivotTableFooterProps) {
  const numberOfRows = props.totals.length;
  return (row: ProjectedRow, rowNumber: number) =>
    (value: { index: string[], metric: string, value: number }): PivotCellProps => {
      const {
        columnFormats = {},
        displayOptions = {},
        defaultNumberFormat = '',
      } = props;

      const style = {
        bottom: `${(numberOfRows - rowNumber - 1) * 28}px`,
      };

      return {
        colorCode: displayOptions[value.metric].colorCodeValues,
        columnIndex: value.index,
        format: displayOptions[value.metric].format || columnFormats[value.metric] || defaultNumberFormat,
        metricName: value.metric,
        rowIndex: row.index,
        style,
        value: value.value,
      };

    };
}

export const PivotTableFooter = React.memo((props: PivotTableFooterProps) => {
  const {
    totals,
  } = props;

  const rows = totals.map(
    (row, rowNumber) => {
      const key = row.index[row.index.length - 1];

      const headerCellProps = mapHeaderCell(props)(row, rowNumber);
      const cellProps = row.data.map(mapValueCells(props)(row, rowNumber));

      const headerCells = headerCellProps.map(
        (cellProp, i) => (<PivotRowHeaderCell key={`th-${key}-${i}`} {...cellProp} />),
      );
      const cells = cellProps.map(
        (cellProp, i) => (
          <PivotCell
            key={`${key}-${i}`}
            {...cellProp}
          />
        ),
      );
      return (
        <tr key={`${row.index.join('-')}-${rowNumber}`} >
          {headerCells}
          {cells}
        </tr>
      );
    });

  return (
    <tfoot>
      {rows}
    </tfoot>
  );
});
