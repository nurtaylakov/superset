import * as classNames from 'classnames';
import * as React from 'react';

import { SortIcon } from './SortIcon';

export interface PivotHeaderCellProps {
    colSpan?: number;
    index: string[];
    metric?: string;
    metricLabel?: string;
    group?: number;
    value: string;
    expanded?: boolean;
    hasChildren?: boolean;
    onExpand?: (index: string[]) => void;
    onSort?: (columnIndex: string[], metric: string) => void;
    sortDirection?: number;
    style?: any;
    className?: string;
    width?: number;
}

export const PivotHeaderCell = React.memo((props: PivotHeaderCellProps) => {
    const { value, className, expanded, hasChildren, style, sortDirection, onSort, ...rest } = props;

    let expander;
    const expandClass = expanded ? 'glyphicon-menu-down' : 'glyphicon-menu-right';
    const classes = classNames(['expand-icon', 'glyphicon', expandClass]);

    const onExpand = () => {

        if (props.onExpand && props.index) {
            props.onExpand(props.index);
        }
    };

    if (hasChildren) {
        expander = <span onClick={onExpand} className={classes} />;
    }

    let sorter;

    if (onSort !== undefined) {
        const onSortFn = () => {
            onSort(props.index, props.metric || '');
        };
        sorter = <SortIcon onSort={onSortFn} sortDirection={sortDirection} />;
    }

    return (
        <th className={classNames(className, 'pivot-th')} style={style} {...rest}>
            <div>{expander}<span>{value}</span>{sorter}</div>
        </th>
    );
});
