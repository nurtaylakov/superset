/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import * as math from 'mathjs';

import {
  EXPRESSION_TYPES,
} from './AdhocMetric';

function getMetricName(metric) {
  if (typeof metric === 'string') {
    return metric;
  }

  return metric.label;
}

export default class CalculatedMetric {
  constructor(calculatedMetric) {
    calculatedMetric = calculatedMetric || {};
    this.expressionType = calculatedMetric.expressionType || EXPRESSION_TYPES.CALCULATED;
    this.expression = calculatedMetric.expression || '';
    this.hasCustomLabel = !!(calculatedMetric.hasCustomLabel && calculatedMetric.label);
    this.label = this.hasCustomLabel ? calculatedMetric.label : this.getDefaultLabel();
    this.fromFormData = !!calculatedMetric.optionName;

    this.availableMetrics = calculatedMetric.availableMetrics || [];

    this.optionName = calculatedMetric.optionName ||
      `metric_${Math.random().toString(36).substring(2, 15)}_${Math.random().toString(36).substring(2, 15)}`;
  }

  compile() {
    if (this.expression !== this.compiledExpression) {
      try {
        this.compiled = math.compile(this.expression);
        this.compiledExpression = this.expression;
        this.lastError = null;
      } catch (error) {
        this.lastError = error;
      }
    }
  }

  getDefaultLabel() {
    const label = this.expression;
    return label.length < 43 ?
      label :
      label.substring(0, 40) + '...';
  }

  getScope() {
    const metrics = this.availableMetrics.reduce((scope, val, i) => {
      const name = getMetricName(val) || `metric${i}`;
      return {
        ...scope,
        [name]: 1,
      };
    }, {});
    return {
      metrics,
    };
  }


  duplicateWith(nextFields) {
    return new CalculatedMetric({
      ...this,
      ...nextFields,
    });
  }

  equals(calculatedMetric) {
    return calculatedMetric.label === this.label &&
      calculatedMetric.expression === this.expression;
  }

  isValid() {
    this.compile();
    if (this.lastError) {
      return false;
    }
    try {
      this.compiled.eval(this.getScope());
    } catch (error) {
      this.lastError = error;
      return false;
    }
    return true;
  }

  calculate(metricValues) {
    const scope = {
      metrics: metricValues,
    };
    try {
      this.compile();
      const result = this.compiled.eval(scope);
      return result;
    } catch (error) {
      return NaN;
    }
  }

}


export function isDictionaryForCalculatedMetric(value) {
  return value && !(value instanceof CalculatedMetric) && value.expressionType === EXPRESSION_TYPES.CALCULATED;
}

// calculated metrics are stored as dictionaries in URL params. We convert them back into the
// CalculatedMetric class for typechecking, consistency and instance method access.
export function coerceCalculatedMetrics(value) {
  if (!value) {
    return [];
  }
  if (!Array.isArray(value)) {
    if (isDictionaryForCalculatedMetric(value)) {
      return [new CalculatedMetric(value)];
    }
    return [value];
  }
  return value.map((val) => {
    if (isDictionaryForCalculatedMetric(val)) {
      return new CalculatedMetric(val);
    }
    return val;
  });
}

