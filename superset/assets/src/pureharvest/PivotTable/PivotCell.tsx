import { formatNumber } from '@superset-ui/number-format';
import * as classNames from 'classnames';
import * as React from 'react';
import { SortIcon } from './SortIcon';

export interface PivotCellProps {
    className?: string;
    columnIndex: string[];
    rowIndex: string[];
    metricName?: string;
    expanded?: boolean;
    format?: string;
    value: number;
    colorCode?: boolean;
    hasChildren?: boolean;
    onSort?: (rowIndex: string[], columnIndex: string[], metricName?: string) => void;
    sortDirection?: number;
    style?: any;
    width?: number;
}

export const PivotCell = React.memo((props: PivotCellProps) => {
    const {
        format,
        value,
        className,
        colorCode,
        expanded,
        hasChildren,
        onSort,
        columnIndex,
        rowIndex,
        metricName,
        sortDirection, ...rest } = props;

    const formattedValue = format !== undefined ? formatNumber(format, value) : value;
    const classes = classNames(className, 'pivot-td', metricName);
    const valueClasses = classNames({
        'negative-value': colorCode && value < 0,
        'positive-value': colorCode && value > 0,
    });
    let sorter;

    if (hasChildren && onSort && expanded) {
        const onClick = () => { onSort(rowIndex, columnIndex, metricName); };
        sorter = <SortIcon sortDirection={sortDirection} onSort={onClick} className={'pull-left'} />;
    }

    return (
        <td className={classes} {...rest}>
            <div>
                {sorter}
                <span className={valueClasses}>{formattedValue.toString()}</span>
            </div>
        </td>
    );
});
