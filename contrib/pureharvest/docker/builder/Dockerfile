FROM python:3.6

RUN useradd --user-group --create-home --no-log-init --shell /bin/bash superset

# Configure environment
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8

RUN apt-get update -y

# Install dependencies to fix `curl https support error` and `elaying package configuration warning`
RUN apt-get install -y apt-transport-https apt-utils

# Install superset dependencies
# https://superset.incubator.apache.org/installation.html#os-dependencies
RUN apt-get install -y build-essential libssl-dev \
    libffi-dev python3-dev libsasl2-dev libldap2-dev libxi-dev

# Install extra useful tool for development
RUN apt-get install -y vim less postgresql-client redis-tools

# Install nodejs for custom build
# https://superset.incubator.apache.org/installation.html#making-your-own-build
# https://nodejs.org/en/download/package-manager/
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get install -y nodejs

WORKDIR /home/superset

# make the cache directories volumes
VOLUME [ "/root/.cache" ]
VOLUME [ "/root/.npm" ]

COPY requirements.txt .
COPY requirements-dev.txt .

ENV PATH=/home/superset/superset/bin:/opt/superset/bin:$PATH
ENV PYTHONPATH=/home/superset/superset:/opt/superset/lib/python3.6/site-packages:$PYTHONPATH

RUN pip install --upgrade setuptools pip && \
    mkdir -p /opt/superset && \
    pip install --prefix=/opt/superset -r requirements.txt && \
    pip install -r requirements-dev.txt 

USER superset

COPY --chown=superset:superset superset/assets/package.json superset/assets/
COPY --chown=superset:superset superset/assets/package-lock.json superset/assets/

RUN cd superset/assets \
    && npm install

COPY --chown=superset:superset ./ ./

USER root
RUN pip install redis==2.10.6 pymssql && pip install -e .

COPY contrib/docker/docker-init.sh .
COPY contrib/pureharvest/docker/builder/docker-entrypoint.sh /entrypoint.sh
COPY contrib/pureharvest/docker/builder/superset_config.py /home/superset/superset/

RUN chmod +x /entrypoint.sh

USER superset

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 8088


