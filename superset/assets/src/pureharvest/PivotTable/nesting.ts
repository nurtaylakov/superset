/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import { cloneDeep } from 'lodash';

export type Indexable = {
    index: string[];
} | string[];

export interface Nested<T extends Indexable> {
    key: string;
    values: T[] | Array<Nested<T>>;
}

export interface NestedWithAggregates<T extends Indexable> extends Nested<T> {
    aggregated: T;
    values: T[] | Array<NestedWithAggregates<T>>;
}

export function isNested<T extends Indexable>(value: T | Nested<T>): value is Nested<T> {
    return value.hasOwnProperty('values') && value.hasOwnProperty('key');
}

export function hasNested<T extends Indexable>(values: T[] | Array<Nested<T>>): values is Array<Nested<T>> {
    if (values === undefined || values.length === 0) {
        return false;
    }
    const value = values[0];
    return isNested(value);
}

export function getNested<T extends Indexable>(
    values: Array<NestedWithAggregates<T>>, index: string[],
): NestedWithAggregates<T> | undefined {
    const key = index[0];
    const rest = index.slice(1);

    for (const value of values) {
        if (value.key === key) {
            if (index.length === 1) {
                return value;
            }
            if (hasNested(value.values)) {
                return getNested(value.values, rest);
            }
        }
    }

    return undefined;
}

export type AggregationFunction<T extends Indexable> = (values: T[], index?: string[]) => T;

export function aggregateNested<T extends Indexable>(aggFn: AggregationFunction<T>, parentIndex: string[] = []) {
    return (value: Nested<T>): NestedWithAggregates<T> => {

        const index = parentIndex.concat(value.key);
        const values = value.values;
        if (!hasNested(values)) {
            return {
                aggregated: aggFn(values, index),
                key: value.key,
                values: cloneDeep(values),
            };
        }

        const aggregatedValues = values.map(aggregateNested(aggFn, index));
        const aggregated = aggFn(aggregatedValues.map((val) => val.aggregated), index);
        return {
            aggregated,
            key: value.key,
            values: aggregatedValues,
        };
    };
}

export interface NestedStates<T> { [key: string]: NestedState<T>; }

export interface NestedState<T> {
    key: string;
    state: T;
    children?: NestedStates<T>;
}

export type UpdateNestedStateFunc<T> = (state: T) => T;

export function updateNestedState<T>(updateFunc: UpdateNestedStateFunc<T>, defaultState?: T) {

    return (states: NestedStates<T>, index: string[]): NestedStates<T> => {
        const key = index[0];
        const rest = index.slice(1);

        const state = states[key] || {
            key,
            state: defaultState,
        };

        if (index.length === 1) {
            return {
                ...states,
                [key]: {
                    ...state,
                    state: updateFunc(state.state),
                },
            };
        }

        return {
            ...states,
            [key]: {
                ...state,
                children: updateNestedState(updateFunc, defaultState)((state.children || {}), rest),
            },
        };
    };
}

export function getState<T>(states: NestedStates<T>, index: string[], defaultState: T): T {
    const key = index[0];
    const rest = index.slice(1);

    const state = states[key] || {
        key,
        state: defaultState,
    };

    if (index.length === 1) {
        return state.state;
    }

    if (state.children === undefined) {
        return defaultState;
    }

    return getState(state.children, rest, defaultState);
}

export type SortFunctionGetter<R> = (index: string[]) =>
    ((a: R, b: R) => number) | undefined;

export function sortNested<T extends Indexable>(
    getSortFn: SortFunctionGetter<T>,
) {
    return (values: Array<NestedWithAggregates<T>>, parentIndex: string[]): Array<NestedWithAggregates<T>> => {
        const sortFn = getSortFn(parentIndex) || (() => 0);
        const sortedValues = values.sort((a, b) => {
            return sortFn(a.aggregated, b.aggregated);
        });
        return sortedValues.map((value) => {
            if (hasNested<T>(value.values)) {
                return {
                    ...value,
                    values: sortNested(getSortFn)(value.values, parentIndex.concat(value.key)),
                };
            }
            return value;
        });
    };
}

export type ProjectionFunction<T, R> = (values: T[], parentIndex?: string[], numberOfChildren?: number) => R[];

export enum AggregatePosition {
    Hidden,
    Before,
    After,
    Replace,
}

export type AggregatePositionGetter = (index: string[]) => AggregatePosition;

export function nestedWithAggregatesReducer<T extends Indexable, R>(
    projectFn: ProjectionFunction<T, R>,
    aggregatePosition: AggregatePosition | AggregatePositionGetter = AggregatePosition.Hidden,
) {

    return (parentIndex: string[]) =>
        (previous: R[], value: NestedWithAggregates<T>, i: number, allValues: Array<NestedWithAggregates<T>>): R[] => {
            const values = value.values;
            const index = parentIndex.concat(value.key);
            const position = typeof aggregatePosition === 'function' ? aggregatePosition(index) : aggregatePosition;
            const numberOfChildren = hasNested(values) ? values.length : 0;

            const projectedAggregate = projectFn([value.aggregated], index, numberOfChildren);

            if (position === AggregatePosition.Replace) {
                return previous.concat(projectedAggregate);
            }

            let projectedValues: R[];

            if (hasNested(values)) {
                projectedValues = values.reduce(nestedWithAggregatesReducer(projectFn, aggregatePosition)(index), []);
            } else if (position === AggregatePosition.Hidden) {
                projectedValues = projectFn(values, index, numberOfChildren);
            } else {
                projectedValues = [];
            }

            if (position === AggregatePosition.Before) {
                return previous.concat(projectedAggregate, projectedValues);
            } else if (position === AggregatePosition.After) {
                return previous.concat(projectedValues, projectedAggregate);
            }

            return previous.concat(projectedValues);
        };
}
