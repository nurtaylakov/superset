#!/usr/bin/env bash
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
VERSION=$(grep version $SCRIPT_DIR/../../superset/assets/package.json | cut -c 15- | rev | cut -c 3- | rev)

set -ex

docker build -t registry.gitlab.com/pureharvest/superset-build:$VERSION -f $SCRIPT_DIR/docker/builder/Dockerfile $SCRIPT_DIR/../../

docker build --build-arg=VERSION=$VERSION -t registry.gitlab.com/pureharvest/superset:$VERSION $SCRIPT_DIR/docker/release/
