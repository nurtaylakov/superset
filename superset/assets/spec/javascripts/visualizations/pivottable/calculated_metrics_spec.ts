/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import CalculatedMetric from '../../../../src/explore/CalculatedMetric';
import {
  calculateMetrics,
} from '../../../../src/pureharvest/PivotTable/transformProps';
import '../../../helpers/shim';

describe('pivot table', () => {

  describe('calculating metrics', () => {
    // const metrics = [{
    //   d3format: null,
    //   description: null,
    //   expression: 'SUM(m1)',
    //   id: 1,
    //   metric_name: 'sum_m1',
    //   verbose_name: null,
    //   waring_text: null,
    // }, {
    //   d3format: null,
    //   description: null,
    //   expression: 'AVG(m2)',
    //   id: 1,
    //   metric_name: 'avg_m2',
    //   verbose_name: null,
    //   waring_text: null,
    // }];

    it('can handle simple calculations', () => {
      const calculatedMetrics = [
        new CalculatedMetric({
            expression: 'metrics["m0"] + metrics["m1"]',
            hasCustomLabel: true,
            label: 'cm1',
        }),
    ];

      const calculated = calculateMetrics({
          m0: 1,
          m1: 2,
      }, calculatedMetrics);

      expect(calculated).toEqual({
          cm1: 3,
      });
    });

    it('can handle multiple calculations', () => {
      const calculatedMetrics = [
        new CalculatedMetric({
            expression: 'metrics["m0"] + metrics["m1"]',
            hasCustomLabel: true,
            label: 'cm1',
        }),
        new CalculatedMetric({
          expression: 'metrics["cm1"] + 5',
          hasCustomLabel: true,
          label: 'cm2',
      }),
    ];
      const calculated = calculateMetrics({
          m0: 1,
          m1: 2,
      }, calculatedMetrics);

      expect(calculated).toEqual({
          cm1: 3,
          cm2: 8,
      });
    });
  });

});
