import * as classNames from 'classnames';
import * as React from 'react';

import { SortIcon } from './SortIcon';

export interface PivotRowHeaderCellProps {
    columnIndex: string[];
    rowIndex: string[];
    metric?: string;
    metricLabel?: string;
    depth?: number;
    value: string;
    expanded?: boolean;
    hasChildren?: boolean;
    onExpand?: (index: string[]) => void;
    onSort?: (rowIndex: string[], columnIndex: string[], metricName?: string) => void;
    sortDirection?: number;
    style?: any;
    className?: string;
    width?: number;
}

export const PivotRowHeaderCell = React.memo((props: PivotRowHeaderCellProps) => {
    const { value, className, expanded, hasChildren, style, sortDirection, onSort } = props;

    let expander;
    const depth = props.depth || 0;
    const expandClass = expanded ? 'glyphicon-menu-down' : 'glyphicon-menu-right';
    const classes = classNames(['expand-icon', 'glyphicon', expandClass]);

    const onExpand = () => {

        if (props.onExpand && props.columnIndex) {
            props.onExpand(props.columnIndex);
        }
    };

    if (hasChildren && props.onExpand) {
        expander = <span onClick={onExpand} className={classes} />;
    }

    let sorter;

    if (onSort !== undefined && hasChildren && expanded) {
        const onSortFn = () => {
            onSort(props.rowIndex, props.columnIndex, props.metric);
        };
        sorter = <SortIcon onSort={onSortFn} sortDirection={sortDirection} className={'pull-right'} />;
    }

    const padding = (depth * 15) - 1;

    return (
        <th className={classNames(className, 'pivot-th')} style={style}>
            <div style={{ paddingLeft: `${padding}px` }}>{expander}{sorter}<span>{value}</span></div>
        </th>
    );
});
