/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, FormGroup, Popover } from 'react-bootstrap';
import VirtualizedSelect from 'react-virtualized-select';
import ace from 'brace';
import AceEditor from 'react-ace';
import 'brace/mode/sql';
import 'brace/theme/github';
import 'brace/ext/language_tools';
import { t } from '@superset-ui/translation';

import OnPasteSelect from '../../components/OnPasteSelect';
import CalculatedMetricEditPopoverTitle from './CalculatedMetricEditPopoverTitle';
import columnType from '../propTypes/columnType';
import CalculatedMetric from '../CalculatedMetric';
import { sqlWords } from '../../SqlLab/components/AceEditorWrapper';

const langTools = ace.acequire('ace/ext/language_tools');

const propTypes = {
  calculatedMetric: PropTypes.instanceOf(CalculatedMetric).isRequired,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onResize: PropTypes.func.isRequired,
  columns: PropTypes.arrayOf(columnType),
  datasourceType: PropTypes.string,
};

const defaultProps = {
  columns: [],
};

const startingWidth = 300;
const startingHeight = 180;

export default class CalculatedMetricEditPopover extends React.Component {
  constructor(props) {
    super(props);
    this.onSave = this.onSave.bind(this);
    this.onExpressionChange = this.onExpressionChange.bind(this);
    this.onLabelChange = this.onLabelChange.bind(this);
    this.onDragDown = this.onDragDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.handleAceEditorRef = this.handleAceEditorRef.bind(this);
    this.refreshAceEditor = this.refreshAceEditor.bind(this);
    this.state = {
      calculatedMetric: this.props.calculatedMetric,
      width: startingWidth,
      height: startingHeight,
    };
    this.selectProps = {
      multi: false,
      name: 'select-column',
      labelKey: 'label',
      autosize: false,
      clearable: true,
      selectWrap: VirtualizedSelect,
    };
    if (langTools) {
      const words = sqlWords.concat(this.props.columns.map(column => (
        { name: column.column_name, value: column.column_name, score: 50, meta: 'column' }
      )));
      const completer = {
        getCompletions: (aceEditor, session, pos, prefix, callback) => {
          callback(null, words);
        },
      };
      langTools.setCompleters([completer]);
    }
    document.addEventListener('mouseup', this.onMouseUp);
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('mousemove', this.onMouseMove);
  }

  onSave() {
    this.props.onChange(this.state.calculatedMetric);
    this.props.onClose();
  }

  onExpressionChange(expression) {
    this.setState({
      calculatedMetric: this.state.calculatedMetric.duplicateWith({
        expression,
      }),
    });
  }

  onLabelChange(e) {
    this.setState({
      calculatedMetric: this.state.calculatedMetric.duplicateWith({
        label: e.target.value, hasCustomLabel: true,
      }),
    });
  }

  onDragDown(e) {
    this.dragStartX = e.clientX;
    this.dragStartY = e.clientY;
    this.dragStartWidth = this.state.width;
    this.dragStartHeight = this.state.height;
    document.addEventListener('mousemove', this.onMouseMove);
  }

  onMouseMove(e) {
    this.props.onResize();
    this.setState({
      width: Math.max(this.dragStartWidth + (e.clientX - this.dragStartX), startingWidth),
      height: Math.max(this.dragStartHeight + (e.clientY - this.dragStartY) * 2, startingHeight),
    });
  }

  onMouseUp() {
    document.removeEventListener('mousemove', this.onMouseMove);
  }

  handleAceEditorRef(ref) {
    if (ref) {
      this.aceEditorRef = ref;
    }
  }

  refreshAceEditor() {
    setTimeout(() => this.aceEditorRef.editor.resize(), 0);
  }

  render() {
    const {
      calculatedMetric: propsCalculatedMetric,
      columns,
      onChange,
      onClose,
      onResize,
      datasourceType,
      ...popoverProps
    } = this.props;

    const { calculatedMetric } = this.state;

    const popoverTitle = (
      <CalculatedMetricEditPopoverTitle
        calculatedMetric={calculatedMetric}
        onChange={this.onLabelChange}
      />
    );

    const stateIsValid = calculatedMetric.isValid();
    const lastError = calculatedMetric.lastError ? calculatedMetric.lastError.toString() : null;
    const hasUnsavedChanges = !calculatedMetric.equals(propsCalculatedMetric);
    const style = {
      width: this.state.width,
      height: this.state.height,
    };

    return (
      <Popover
        id="calculated-metrics-edit-popover"
        title={popoverTitle}
        {...popoverProps}
      >
        <div style={style}>
          <FormGroup>
            <AceEditor
              ref={this.handleAceEditorRef}
              theme="github"
              height={(this.state.height - 43) + 'px'}
              onChange={this.onExpressionChange}
              width="100%"
              showGutter={false}
              value={calculatedMetric.expression}
              editorProps={{ $blockScrolling: true }}
              className="calculated-metric-expression-editor"
              wrapEnabled
            />
          </FormGroup>
        </div>
        <div>
          { lastError }
        </div>
        <div>
          <Button
            disabled={!stateIsValid}
            bsStyle={(hasUnsavedChanges && stateIsValid) ? 'primary' : 'default'}
            bsSize="small"
            className="m-r-5"
            onClick={this.onSave}
          >
            Save
          </Button>
          <Button bsSize="small" onClick={this.props.onClose}>Close</Button>
          <i onMouseDown={this.onDragDown} className="glyphicon glyphicon-resize-full edit-popover-resize" />
        </div>
      </Popover>
    );
  }
}
CalculatedMetricEditPopover.propTypes = propTypes;
CalculatedMetricEditPopover.defaultProps = defaultProps;
