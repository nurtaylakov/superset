import { flatMap } from 'lodash';

import CalculatedMetric from '../../explore/CalculatedMetric';
import { MetricPosition } from './constants';
import {
  AggregatePosition,
  AggregatePositionGetter,
  getNested,
  NestedWithAggregates,
  nestedWithAggregatesReducer,
  sortNested,
} from './nesting';
import { ColumnWithMetrics, getMetricName, Metric, MetricDisplayOptions, Row } from './transformProps';

export function isRowMetrics(position: number) {
  return position === MetricPosition.Rows || position === MetricPosition.RowsCombined;
}

export type RowSortFunctionGetter = (index: string[]) => (a: Row, b: Row) => number;

export interface ProjectedRow {
  groupIndex?: number;
  index: string[];
  data: Array<{ index: string[]; value: number; metric?: string }>;
  metric?: string;
  numberOfChildren?: number;
}

export function getValueForSort(columnIndex: string[], metricName: string) {
  return (row: Row): number => {

    const value = getNested(row.columns, columnIndex);

    if (value) {
      return value.aggregated.metrics[metricName] || 0;
    }

    return 0;
  };
}

export function projectData(
  metrics: Array<Metric | CalculatedMetric>,
  displayOptions: { [key: string]: MetricDisplayOptions },
  metricsPosition = MetricPosition.ColumnsCombined,
  aggregatePosition: AggregatePosition | AggregatePositionGetter = AggregatePosition.After,
  columnAggregatePosition: AggregatePosition | AggregatePositionGetter = AggregatePosition.After,
  getRowSortFn?: RowSortFunctionGetter,
) {
  const sortFn = getRowSortFn ? getRowSortFn : () => () => 0;
  switch (metricsPosition) {
    case MetricPosition.Columns:
      return (rows: Array<NestedWithAggregates<Row>>) => {
        const sortedRows = sortNested(sortFn)(rows, []);
        const projectionFn = projectMetricsAsColumns(metrics, false, columnAggregatePosition);
        const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition)([]);
        return sortedRows.reduce(reducer, []);
      };
    case MetricPosition.ColumnsCombined:
      return (rows: Array<NestedWithAggregates<Row>>) => {
        const sortedRows = sortNested(sortFn)(rows, []);
        const projectionFn = projectMetricsAsColumns(metrics, true, columnAggregatePosition);
        const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition)([]);
        return sortedRows.reduce(reducer, []);
      };
    case MetricPosition.Rows:
      return (rows: Array<NestedWithAggregates<Row>>) => {
        const sortedRows = sortNested(sortFn)(rows, []);
        const projectionFn = projectMetricsAsRows(metrics, false, columnAggregatePosition);
        const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition)([]);
        return sortedRows.reduce(reducer, []);
      };
    case MetricPosition.RowsCombined:
      return (rows: Array<NestedWithAggregates<Row>>) => {
        const sortedRows = sortNested(sortFn)(rows, []);
        const projectionFn = projectMetricsAsRows(metrics, true, columnAggregatePosition);
        const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition)([]);
        return sortedRows.reduce(reducer, []);
      };
  }

  return (rows: Array<NestedWithAggregates<Row>>) => {
    const sortedRows = sortNested(sortFn)(rows, []);
    const projectionFn = projectMetricsAsColumns(metrics, true, columnAggregatePosition);
    const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition)([]);
    return sortedRows.reduce(reducer, []);
  };
}

export function projectMetricsAsRows(
  metrics: Array<Metric | CalculatedMetric>,
  combineMetrics: boolean,
  aggregatePosition: AggregatePosition | AggregatePositionGetter = AggregatePosition.After,
) {
  return (rows: Row[], parentIndex: string[], numberOfChildren?: number): ProjectedRow[] => {
    return flatMap(rows, (row) => {
      return metrics.map((metric, i) => {
        const name = getMetricName(metric);
        const projectionFn = (columns: ColumnWithMetrics[], columnIndex: string[], columnNumberOfChildren: number) => {
          return columns.map((column) => ({
            index: column.index,
            metric: name,
            numberOfChildren: columnNumberOfChildren,
            value: column.metrics[name],
          }));
        };
        const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition);
        return {
          data: row.columns.reduce(reducer([]), []),
          index: row.index,
          metric: name,
          numberOfChildren,
        };
      });
    });
  };
}

export function projectMetricsAsColumns(
  metrics: Array<Metric | CalculatedMetric>,
  combineMetrics: boolean,
  aggregatePosition: AggregatePosition | AggregatePositionGetter = AggregatePosition.After,
  getRowSortFn?: RowSortFunctionGetter,
) {
  if (combineMetrics) {
    return (rows: Row[], parentIndex: string[], numberOfChildren?: number, groupIndex?: number): ProjectedRow[] => {
      const projectionFn = (columns: ColumnWithMetrics[], columnIndex: string[], columnNumberOfChildren: number) => {
        return flatMap(columns, (column) => metrics.map((metric) => {
          const name = getMetricName(metric);
          return {
            index: column.index,
            metric: name,
            numberOfChildren: columnNumberOfChildren,
            value: column.metrics[name],
          };
        }));
      };
      const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition);

      return rows.map((row, i) => {
        return {
          data: row.columns.reduce(reducer([]), []),
          index: row.index,
          numberOfChildren,
        };
      });
    };
  }

  return (rows: Row[], parentIndex: string[], numberOfChildren?: number, groupIndex?: number): ProjectedRow[] => {

    return rows.map((row, i) => {
      const data = flatMap(metrics, (metric) => {
        const name = getMetricName(metric);
        const projectionFn = (columns: ColumnWithMetrics[], columnIndex: string[], columnNumberOfChildren: number) => {
          return columns.map((column) => {
            return {
              index: column.index,
              metric: name,
              numberOfChildren: columnNumberOfChildren,
              value: column.metrics[name],
            };
          });
        };

        const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition);
        return row.columns.reduce(reducer([]), []);
      });
      return {
        data,
        groupIndex,
        index: row.index,
        numberOfChildren,
      };
    });
  };
}

export interface ProjectedColumn {
  index: string[];
  metrics: Array<Metric | CalculatedMetric>;
}

export function projectColumnHeaders(
  columns: Array<NestedWithAggregates<string[]>>,
  columnNames: string[],
  metrics: Array<Metric | CalculatedMetric>,
  metricsPosition = MetricPosition.ColumnsCombined,
  aggregatePosition = AggregatePosition.After,
): ProjectedColumn[] {

  if (metricsPosition === MetricPosition.Rows || metricsPosition === MetricPosition.RowsCombined) {
    if (columns.length === 0) {
      return [{
        index: [''],
        metrics,
      }];
    }

    return columns.reduce(nestedWithAggregatesReducer((indexes: string[][]) => {
      return indexes.map((index) => {
        const indexFill = columnNames.length - index.length;
        let filledIndex = index;
        if (indexFill > 0) {
          filledIndex = index.concat(new Array(indexFill).fill('All'));
        }
        return {
          index: filledIndex,
          metrics,
        };
      });
    }, aggregatePosition)([]), []);
  }

  if (columns.length === 0) {
    return metrics.map((metric) => ({
      index: [''],
      metrics: [metric],
    }));
  }

  if (metricsPosition === MetricPosition.ColumnsCombined) {
    return columns.reduce(nestedWithAggregatesReducer((indexes: string[][]) => {
      return flatMap(metrics, (metric) => indexes.map((index) => {
        const indexFill = columnNames.length - index.length;

        if (indexFill > 0) {
          return {
            index: index.concat(new Array(indexFill).fill('All')),
            metrics: [metric],
          };
        }

        return {
          index,
          metrics: [metric],
        };
      }));
    }, aggregatePosition)([]), []);
  }

  return flatMap<Metric | CalculatedMetric, ProjectedColumn>(metrics, (metric, i) => {
    return columns.reduce(nestedWithAggregatesReducer((indexes: string[][]) => {
      return indexes.map((index) => {
        const indexFill = columnNames.length - index.length;
        let filledIndex = index;
        if (indexFill > 0) {
          filledIndex = index.concat(new Array(indexFill).fill('All'));
        }
        return {
          index: filledIndex,
          metrics: [metric],
        };
      });
    }, aggregatePosition)([]), []);
  });
}
