import * as classNames from 'classnames';
import * as React from 'react';

export interface SortIconProps extends React.HTMLProps<any> {
    onSort: () => void;
    sortDirection?: number;
}

export const SortIcon = React.memo((props: SortIconProps) => {
    const { onSort, sortDirection, className, ...rest } = props;
    const classes = classNames(className, {
        'glyphicon': true,
        'glyphicon-sort': true,
        'glyphicon-sort-by-attributes': sortDirection === 1,
        'glyphicon-sort-by-attributes-alt': sortDirection === -1,
        'sort-icon': true,
    });

    return (
        <span onClick={onSort} className={classes} {...rest} />
    );
});
