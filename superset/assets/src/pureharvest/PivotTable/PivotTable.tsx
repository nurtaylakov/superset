import { t } from '@superset-ui/translation';
import { isEqual, keyBy } from 'lodash';
import * as React from 'react';

import CalculatedMetric from 'src/explore/CalculatedMetric';
import { MetricPosition } from './constants';
import { AggregatePosition, getState, NestedStates, NestedWithAggregates, updateNestedState } from './nesting';
import { PivotHeader } from './PivotHeader';
import { PivotRow } from './PivotRow';
import './PivotTable.css';
import { PivotTableFooter } from './PivotTableFooter';
import { getValueForSort, projectData } from './projection';
import { getMetricName, IndexKey, Metric, MetricDisplayOptions, Row } from './transformProps';

export interface PivotData {
  rows: Array<NestedWithAggregates<Row>>;
  columns: Array<NestedWithAggregates<IndexKey>>;
  data: number[][];
  index: string[][];
}

export interface PivotTableProps {
  data: PivotData;
  height: number;
  width: number;
  calculatedMetrics: CalculatedMetric[];
  columns: string[];
  columnFormats: { [key: string]: string; };
  combineMetric: any;
  metricDisplayOptions: MetricDisplayOptions[];
  metricPosition?: number;
  metrics: Metric[];
  numberFormat: string;
  numGroups: number;
  groupby: string[];
  verboseMap: { [key: string]: string; };
}

interface RowState {
  expanded: boolean;
  sortColumn: string[];
  sortDirection: number;
  sortMetric: string;
}

const defaultRowState = {
  expanded: false,
  sortColumn: [],
  sortDirection: 1,
  sortMetric: '',
};

interface State {
  sortColumn: string[];
  sortDirection: number;
  sortMetric?: string;
  rowStates: NestedStates<RowState>;
}

function getSortFn(sortColumn: string[], sortDirection: number, sortMetric: string) {
  return (a: Row, b: Row) => {
    if ((sortColumn === undefined || sortColumn.length === 0) && !sortMetric) {
      const aKey = a.index[a.index.length - 1];
      const bKey = b.index[b.index.length - 1];

      return aKey.localeCompare(bKey) * sortDirection;
    }

    const aVal = getValueForSort(sortColumn, sortMetric)(a);
    const bVal = getValueForSort(sortColumn, sortMetric)(b);

    if (isNaN(aVal)) {
      return 1;
    }

    if (isNaN(bVal)) {
      return -1;
    }

    if (aVal < bVal) {
      return -1 * sortDirection;
    }

    if (aVal > bVal) {
      return sortDirection;
    }
    return 0;
  };
}

function getRowSortFn(
  rowStates: NestedStates<RowState>, rootSortColumn: string[], rootSortDirection: number, rootSortMetric: string,
) {
  return (index: string[]) => {

    if (index.length === 0) {
      return getSortFn(rootSortColumn, rootSortDirection, rootSortMetric);
    }

    const state = getState(rowStates, index, {
      expanded: false,
      sortColumn: [],
      sortDirection: 1,
      sortMetric: '',
    });

    const { sortColumn, sortDirection, sortMetric } = state;

    return getSortFn(sortColumn, sortDirection, sortMetric);
  };

}

function getRowAggregatePosition(rowStates: NestedStates<RowState>) {
  return (index: string[]) => {
    if (isEqual(index, ['All'])) {
      return AggregatePosition.Hidden;
    }

    const state = getState(rowStates, index, {
      expanded: false,
      sortColumn: [],
      sortDirection: 1,
      sortMetric: '',
    });

    return state.expanded ? AggregatePosition.Before : AggregatePosition.Replace;
  };
}

class PivotTable extends React.Component<PivotTableProps, State> {

  constructor(props: PivotTableProps) {
    super(props);
    this.state = {
      rowStates: {},
      sortColumn: [],
      sortDirection: 1,
    };

    this.sortOn = this.sortOn.bind(this);
    this.onRowSort = this.onRowSort.bind(this);
    this.onExpand = this.onExpand.bind(this);
  }

  public sortOn(column: string[], metric: string) {
    this.onRowSort(['All'], column, metric);
  }

  public onExpand(index: string[]) {
    const rowStates = updateNestedState<RowState>((state) => {
      return {
        ...state,
        expanded: !state.expanded,
      };
    }, defaultRowState)(this.state.rowStates, index);

    this.setState({
      rowStates,
    });
  }

  public onRowSort(rowIndex: string[], columnIndex: string[], metric: string) {
    const rowStates = updateNestedState<RowState>((state) => {
      let sortDirection = state.sortDirection;

      if (isEqual(state.sortColumn, columnIndex)) {
        sortDirection = sortDirection * -1;
      }
      return {
        ...state,
        sortColumn: columnIndex,
        sortDirection,
        sortMetric: metric,
      };
    }, defaultRowState)(this.state.rowStates, rowIndex);

    this.setState({
      rowStates,
    });
  }

  public render() {
    const {
      data, columns, groupby, combineMetric, metrics, calculatedMetrics, columnFormats,
      numberFormat,
      metricDisplayOptions: metricOrder,
      metricPosition = MetricPosition.ColumnsCombined,
    } = this.props;

    if (data) {
      const { rowStates } = this.state;
      const allMetrics = [...(metrics || []), ...(calculatedMetrics || [])];
      let sortedMetrics = allMetrics;
      const metricDisplayOptions = keyBy<MetricDisplayOptions>(metricOrder, (metric) => metric.name);

      if (metricOrder) {
        const metricMap = keyBy(allMetrics, getMetricName);
        sortedMetrics = metricOrder.map( (metric) => metricMap[metric.name]);
      }

      const projectionFn = projectData(
        sortedMetrics, metricDisplayOptions,
        metricPosition,
        getRowAggregatePosition(this.state.rowStates),
        AggregatePosition.After,
        getRowSortFn(rowStates, this.state.sortColumn, this.state.sortDirection, this.state.sortMetric || ''),
      );

      const sortedRows = projectionFn(data.rows);

      const rows = sortedRows.map(
        (row, i, projectedRows) => {
          const previousRowIndex = i > 0 ? projectedRows[i - 1].index : [];
          const rowState = getState(this.state.rowStates, row.index, defaultRowState);
          const key = row.index.join('-') + (row.metric || '');
          return (
            <PivotRow
              key={key}
              expanded={rowState.expanded}
              index={row.index}
              metrics={sortedMetrics}
              data={row}
              displayOptions={metricDisplayOptions}
              columnFormats={columnFormats}
              combineMetrics={combineMetric}
              onExpand={this.onExpand}
              onSort={this.onRowSort}
              sortColumn={rowState.sortColumn}
              sortDirection={rowState.sortDirection}
              sortMetric={rowState.sortMetric}
              metricPosition={metricPosition}
              showKey={!isEqual(previousRowIndex, row.index) || !row.metric}
              defaultNumberFormat={numberFormat}
            />
          );
        },
      );

      const projectTotals = projectData(
        sortedMetrics, metricDisplayOptions,
        metricPosition,
        AggregatePosition.Replace,
        AggregatePosition.After,
      );

      const totalsRows = projectTotals(data.rows);

      const totalsRowState = getState(this.state.rowStates, ['All'], defaultRowState);
      const classes = 'pivot-table datatable table table-condensed table-striped table-bordered table-hover';

      const style = {
        height: this.props.height - 20,
        overflow: 'auto',
        width: this.props.width,
      };

      return (
        <div style={style}>
          <table className={classes} >
            <PivotHeader
              columnNames={columns}
              columns={data.columns}
              displayOptions={metricDisplayOptions}
              metrics={sortedMetrics}
              metricPosition={metricPosition}
              combineMetrics={combineMetric}
              groupby={groupby}
              sortColumn={totalsRowState.sortColumn}
              sortDirection={totalsRowState.sortDirection}
              sortMetric={totalsRowState.sortMetric}
              onSort={this.sortOn}
            />
            <tbody>
              {rows}
            </tbody>
            <PivotTableFooter
              metricPosition={metricPosition}
              metrics={sortedMetrics}
              totals={totalsRows}
              columnFormats={columnFormats}
              defaultNumberFormat={numberFormat}
              displayOptions={metricDisplayOptions}
            />
          </table>
        </div>
      );
    }
    return (
      <div>{t('Sorry, there appears to be no data')}</div>
    );
  }

}

export default PivotTable;
