import * as classNames from 'classnames';
import { filter, isEqual, range } from 'lodash';
import * as React from 'react';

import CalculatedMetric from 'src/explore/CalculatedMetric';
import { NestedWithAggregates } from './nesting';

import { PivotHeaderCell, PivotHeaderCellProps } from './PivotHeaderCell';
import { isRowMetrics, projectColumnHeaders } from './projection';
import { getMetricLabel, getMetricName, Metric, MetricDisplayOptions } from './transformProps';

export interface PivotHeaderProps {
    combineMetrics?: boolean;
    metrics: Array<Metric | CalculatedMetric>;
    displayOptions?: { [key: string]: MetricDisplayOptions };
    metricPosition: number;
    columnNames: string[];
    columns: Array<NestedWithAggregates<string[]>>;
    groupby: string[];
    sortColumn: string[];
    sortDirection?: number;
    sortMetric?: string;
    className?: string;
    onSort?: (column: string[], metric: string) => void;
    width?: number;
}

export const PivotHeader = React.memo((props: PivotHeaderProps) => {
    const {
        groupby, sortMetric, columns, columnNames, metrics, onSort, sortColumn, sortDirection, metricPosition,
        displayOptions = {},
    } = props;

    const hasMetricGroups = filter(displayOptions, 'group').length > 0;

    const headerRows = projectColumnHeaders(columns, columnNames, metrics, metricPosition);

    const numberOfRows = headerRows[0].index.length;

    const groupbyLabel = groupby.join(' > ');
    const metricOffset = isRowMetrics(metricPosition) ? metrics.length : 1;
    const metricGroupOffset = !isRowMetrics(metricPosition) && hasMetricGroups ? 1 : 0;
    const groupbyLength = numberOfRows + metricOffset + metricGroupOffset;
    const groupByLabels: PivotHeaderCellProps[] = range(groupbyLength).map((v, i) => ({
        className: 'column-header row-header',
        colSpan: isRowMetrics(metricPosition) ? 2 : 1,
        index: [],
        onSort: i === groupbyLength - 1 ? onSort : undefined,
        sortDirection: isEqual(sortColumn, []) && sortMetric === '' ? sortDirection : 0,
        style: {
            top: `${(i) * 28 - 1}px`,
        },
        value: i === groupbyLength - 1 ? groupbyLabel : '',
    }));

    const columnHeaders: PivotHeaderCellProps[][] = headerRows.map(
        (column, columnNumber) => {
            let cells = column.index.map(
                (v, rowNumber) => {
                    const commonProps = {
                        className: classNames({
                            'column-header': true,
                        }),
                        index: column.index,
                        style: {
                            top: `${rowNumber * 28}px`,
                        },
                        value: v,
                    };

                    return commonProps;
                },
            );

            if (hasMetricGroups && !isRowMetrics(metricPosition)) {
                const metricGroupCells = column.metrics.map((metric, i) => {
                    const metricName = getMetricName(metric);
                    const metricDisplayOptions = displayOptions[metricName] || {};
                    const commonProps = {
                        className: classNames({
                            'column-header': true,
                        }),
                        index: column.index,
                        metric: metricName,
                        style: {
                            top: `${(i + numberOfRows) * 28 - 1}px`,
                        },
                        value: metricDisplayOptions.group || '',
                    };

                    return commonProps;
                });
                cells = cells.concat(metricGroupCells);
            }

            const metricCells = column.metrics.map((metric, i) => {
                const metricName = getMetricName(metric);
                const metricDisplayOptions = displayOptions[metricName] || {};
                const commonProps = {
                    className: classNames({
                        'column-header': true,
                    }),
                    index: column.index,
                    metric: metricName,
                    onSort,
                    sortDirection: isEqual(sortColumn, column.index) && sortMetric === metricName ? sortDirection : 0,
                    style: {
                        top: `${(i + numberOfRows + metricGroupOffset) * 28}px`,
                    },
                    value: metricDisplayOptions.label || getMetricLabel(metric),
                };
                return commonProps;
            });

            return cells.concat(metricCells);
        });

    const h = [groupByLabels].concat(columnHeaders).reduce((rows: PivotHeaderCellProps[][], column, columnNumber) => {
        for (let i = 0; i < column.length; i++) {
            const cell = column[i];

            if (rows[i] === undefined) {
                rows[i] = [cell];
            } else {
                const previousCell = rows[i][rows[i].length - 1];

                if (previousCell && previousCell.value === cell.value && isEqual(previousCell.index, cell.index)) {
                    previousCell.colSpan = (previousCell.colSpan || 1) + 1;
                } else {
                    rows[i] = rows[i].concat(cell);
                }
            }

        }

        return rows;
    }, []).map((row, rowNumber) => {
        const cells = row.map((cellProps, i) => {
            return (<PivotHeaderCell key={i} {...cellProps} />);
        });

        return (
            <tr key={rowNumber}>
                {cells}
            </tr>
        );
    });

    return (
        <thead>
            {h}
        </thead>
    );
});
