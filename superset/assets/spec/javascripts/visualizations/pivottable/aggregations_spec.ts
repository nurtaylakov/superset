/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import { aggregateNested } from '../../../../src/pureharvest/PivotTable/nesting';
import {
  aggregateColumns,
  transformData,
} from '../../../../src/pureharvest/PivotTable/transformProps';
import '../../../helpers/shim';

describe('pivot table', () => {

  describe('column aggregating', () => {
    const metrics = [{
      d3format: null,
      description: null,
      expression: 'SUM(m1)',
      id: 1,
      metric_name: 'sum_m1',
      verbose_name: null,
      waring_text: null,
    }, {
      d3format: null,
      description: null,
      expression: 'AVG(m2)',
      id: 1,
      metric_name: 'avg_m2',
      verbose_name: null,
      waring_text: null,
    }];

    const sourceColumns = [
      {
        key: 'a1',
        values: [{
          key: 'b1',
          values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 1, avg_m2: 5} }],
        }, {
          key: 'b2',
          values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 2, avg_m2: 6} }],
        }],
      },
      {
        key: 'a2',
        values: [{
          key: 'b1',
          values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 3, avg_m2: 7} }],
        }, {
          key: 'b2',
          values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 4, avg_m2: 8} }],
        }],
      },
    ];

    it('sums columns', () => {

      const aggFn = aggregateColumns(metrics, []);

      const aggregated = sourceColumns.map(aggregateNested(aggFn));

      expect(aggregated).toEqual([
        {
          aggregated: { index: ['a1'], metrics: { sum_m1: 3, avg_m2: 5.5 } },
          key: 'a1',
          values: [{
            aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 1, avg_m2: 5} },
            key: 'b1',
            values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 1, avg_m2: 5} }],
          }, {
            aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 2, avg_m2: 6} },
            key: 'b2',
            values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 2, avg_m2: 6} }],
          }],
        },
        {
          aggregated: { index: ['a2'], metrics: { sum_m1: 7, avg_m2: 7.5} },
          key: 'a2',
          values: [{
            aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 3, avg_m2: 7} },
            key: 'b1',
            values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 3, avg_m2: 7} }],
          }, {
            aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 4, avg_m2: 8} },
            key: 'b2',
            values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 4, avg_m2: 8} }],
          }],
        },
      ]);
    });
  });

  describe('row aggregating', () => {

    it('can aggregate', () => {
      const data = {
        columns: ['a', 'b'],
        metrics: [{
          d3format: null,
          description: null,
          expression: 'SUM(m1)',
          id: 1,
          metric_name: 'sum_m1',
          verbose_name: null,
          waring_text: null,
        }],
        tableData: {
          columns: [
            ['sum_m1', 'a1', 'b1'],
            ['sum_m1', 'a1', 'b2'],
            ['sum_m1', 'a2', 'b1'],
            ['sum_m1', 'a2', 'b2'],
          ],
          data: [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
          ],
          index: ['r1', 'r2', 'r3', 'r4'],
        },
      };

      const transformedData = transformData(data.tableData, data.columns, data.metrics, []);

      expect(transformedData.columns).toEqual([
        {
          aggregated: ['a1'],
          key: 'a1',
          values: [
            { aggregated: ['a1', 'b1'], key: 'b1', values: [['a1', 'b1']] },
            { aggregated: ['a1', 'b2'], key: 'b2', values: [['a1', 'b2']] },
          ],
        },
        {
          aggregated: ['a2'],
          key: 'a2',
          values: [
            { aggregated: ['a2', 'b1'], key: 'b1', values: [['a2', 'b1']] },
            { aggregated: ['a2', 'b2'], key: 'b2', values: [['a2', 'b2']] },
          ],
        },
      ]);

      expect(transformedData.rows).toEqual([
        {
          aggregated: {
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 3 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 1 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 1 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 2 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 2 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 7 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 3 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 3 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 4 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 4 } }],
                  },
                ],
              },
            ],
            index: ['r1'],
          },
          key: 'r1',
          values: [
            {
              columns: [
                {
                  aggregated: { index: ['a1'], metrics: { sum_m1: 3 } },
                  key: 'a1',
                  values: [
                    {
                      aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 1 } },
                      key: 'b1',
                      values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 1 } }],
                    },
                    {
                      aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 2 } },
                      key: 'b2',
                      values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 2 } }],
                    },
                  ],
                },
                {
                  aggregated: { index: ['a2'], metrics: { sum_m1: 7 } },
                  key: 'a2',
                  values: [
                    {
                      aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 3 } },
                      key: 'b1',
                      values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 3 } }],
                    },
                    {
                      aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 4 } },
                      key: 'b2',
                      values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 4 } }],
                    },
                  ],
                },
              ],
              index: ['r1'],
            },
          ],
        },
        {
          aggregated: {
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 11 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 5 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 5 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 6 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 6 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 15 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 7 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 7 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 8 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 8 } }],
                  },
                ],
              },
            ],
            index: ['r2'],
          },
          key: 'r2',
          values: [{
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 11 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 5 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 5 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 6 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 6 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 15 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 7 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 7 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 8 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 8 } }],
                  },
                ],
              },
            ],
            index: ['r2'],
          }],
        },
        {
          aggregated: {
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 19 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 9 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 9 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 10 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 10 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 23 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 11 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 11 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 12 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 12 } }],
                  },
                ],
              },
            ],
            index: ['r3'],
          },
          key: 'r3',
          values: [{
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 19 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 9 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 9 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 10 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 10 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 23 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 11 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 11 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 12 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 12 } }],
                  },
                ],
              },
            ],
            index: ['r3'],
          }],
        },
        {
          aggregated: {
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 27 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 13 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 13 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 14 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 14 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 31 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 15 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 15 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 16 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 16 } }],
                  },
                ],
              },
            ],
            index: ['r4'],
          },
          key: 'r4',
          values: [{
            columns: [
              {
                aggregated: { index: ['a1'], metrics: { sum_m1: 27 } },
                key: 'a1',
                values: [
                  {
                    aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 13 } },
                    key: 'b1',
                    values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 13 } }],
                  },
                  {
                    aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 14 } },
                    key: 'b2',
                    values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 14 } }],
                  },
                ],
              },
              {
                aggregated: { index: ['a2'], metrics: { sum_m1: 31 } },
                key: 'a2',
                values: [
                  {
                    aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 15 } },
                    key: 'b1',
                    values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 15 } }],
                  },
                  {
                    aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 16 } },
                    key: 'b2',
                    values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 16 } }],
                  },
                ],
              },
            ],
            index: ['r4'],
          }],
        },
      ]);

    });
  });

});
