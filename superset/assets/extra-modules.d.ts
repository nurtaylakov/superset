declare module '@superset-ui/number-format' {
    export function formatNumber(format: string, value: number): string;
};

declare module '@superset-ui/translation' {
    export function t(message: string): string;
};
