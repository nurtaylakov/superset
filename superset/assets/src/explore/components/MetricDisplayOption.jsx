/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import { t } from '@superset-ui/translation';

import InfoTooltipWithTrigger from '../../components/InfoTooltipWithTrigger';
import FormRow from '../../components/FormRow';
import CheckboxControl from './controls/CheckboxControl';
import TextControl from './controls/TextControl';
import ColorPickerControl from './controls/ColorPickerControl';

const propTypes = {
  onChange: PropTypes.func,
  group: PropTypes.string,
  colorCodeValues: PropTypes.bool,
  color: PropTypes.string,
  format: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
};

const defaultProps = {
  onChange: () => {},
  asc: true,
  clearable: true,
  multiple: true,
};

const STYLE_WIDTH = { width: 350 };

export default class MetricDisplayOption extends React.Component {
  constructor(props) {
    super(props);
    const { colorCodeValues, color, format, group, label, name } = props;
    const state = { colorCodeValues, color, format, group, label, name };
    this.state = state;
    this.onChange = this.onChange.bind(this);
    this.onControlChange = this.onControlChange.bind(this);
  }
  onChange() {
    this.props.onChange(this.state);
  }
  onControlChange(attr, value) {
    this.setState({ [attr]: value }, this.onChange);
  }
  setType() {
  }
  textSummary() {
    let label = this.state.label || 'N/A';

    if (label !== this.state.name) {
      label = label + '(' + this.state.name + ')';
    }

    if (this.state.group) {
      label = this.state.group + ': ' + label;
    }
    return label;
  }
  renderForm() {
    return (
      <div>
        <FormRow
          label={t('Label')}
          control={
            <TextControl
              value={this.state.label}
              name="label"
              onChange={v => this.onControlChange('label', v)}
            />
          }
        />
        <FormRow
          label={t('Group')}
          tooltip={t(
            '(optional) Add metric to a group, metrics in the '
            + 'same group that are next to each other will be joined',
          )}
          control={
            <TextControl
              value={this.state.group}
              name="group"
              onChange={v => this.onControlChange('group', v)}
            />
          }
        />
        <FormRow
          label={t('Format')}
          tooltip={t(
            '(optional) d3 format string for this metric ',
          )}
          control={
            <TextControl
              value={this.state.format}
              name="format"
              onChange={v => this.onControlChange('format', v)}
            />
          }
        />
        <FormRow
          label={t('Backgorund Color')}
          tooltip={t('Background color for the metric')}
          control={
            <ColorPickerControl
              value={this.state.color}
              onChange={v => this.onControlChange('color', v)}
            />
          }
        />
        <FormRow
          label={t('Colour Code Values')}
          isCheckbox
          tooltip={t(
            'Multiple selections allowed, otherwise filter ' +
            'is limited to a single value')}
          control={
            <CheckboxControl
              value={this.state.colorCodeValues}
              onChange={v => this.onControlChange('colorCodeValues', v)}
            />
          }
        />
      </div>);
  }
  renderPopover() {
    return (
      <Popover id="ts-col-popo" title={t('Metric Display Configuration')}>
        <div style={STYLE_WIDTH}>
          {this.renderForm()}
        </div>
      </Popover>
    );
  }
  render() {
    return (
      <span>
        {this.textSummary()}{' '}
        <OverlayTrigger
          container={document.body}
          trigger="click"
          rootClose
          ref="trigger"
          placement="right"
          overlay={this.renderPopover()}
        >
          <InfoTooltipWithTrigger
            icon="edit"
            className="text-primary"
            label="edit-ts-column"
          />
        </OverlayTrigger>
      </span>
    );
  }
}

MetricDisplayOption.propTypes = propTypes;
MetricDisplayOption.defaultProps = defaultProps;
